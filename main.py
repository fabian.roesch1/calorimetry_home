from functions import m_json
from functions import m_pck

#get metadata path
path = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
pathnewt = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path)
metadatanewt = m_json.get_metadata_from_setup(pathnewt)
#print(metadata)

# serialnbr hinzufügen
m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata)
m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadatanewt)
print(metadata)

#sensoren überprüfen
m_pck.check_sensors()

#json erstellen
m_json.archiv_json('/home/pi/calorimetry_home/datasheets', pathnewt, '/home/pi/calorimetry_home/log/Newton')
m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path, '/home/pi/calorimetry_home/log/Heat_capacity')

#get Data
data = m_pck.get_meas_data_calorimetry(metadata)
datanewt = m_pck.get_meas_data_calorimetry(metadatanewt)

#abspeicher in json
m_pck.logging_calorimetry(data, metadata, '/home/pi/calorimetry_home/log/Heat_capacity', '/home/pi/calorimetry_home/datasheets')
m_pck.logging_calorimetry(data, metadatanewt, '/home/pi/calorimetry_home/log/Newton', '/home/pi/calorimetry_home/datasheets')